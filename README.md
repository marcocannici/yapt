# YAPT: Yet Another PyTorch Trainer

A simple framework that takes inspiration from all the best outside.
From an idea of Marco Cannici [PyTorch trainer](https://gitlab.com/airlab-404/pytorch-trainer)
Mostly inspired form [PyTorch Lightning](https://github.com/PyTorchLightning/), but better suited for our purposes.

We developed this library to help (force) ourselves and our students to be organized when writing code and designing experiments.

The goal is to have a non-monolithic code base that is easily debuggable and scalable.
We have a seemless integration with [Neptune AI](https://ui.neptune.ai/) to track and compare experiments.

### Future integrations:
- [APEX](https://github.com/NVIDIA/apex)

